 <?php
/*
Template Name: Homepage

*/
?>

<?php get_header(); ?>

<section id="section1">

  <div class="container">
  
  	<div class="row">

	<div class="col-md-8 col-md-offset-2">
    
   	 <img src="<?php bloginfo('template_url');?>/images/marian-intro.svg">
    
    </div>
    
    </div>
    
  </div>

</section>

<section id="section2">

<div class="container">
  
  	<div class="row">

	<div class="col-md-8 col-md-offset-2" style="margin-top:100px; margin-bottom: 100px;">
    
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <p>
    
        
        <?php the_content() ?>
        
      
    </p>

      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, there are no posts.'); ?>
      </p>
      <?php endif; ?>
    
    
    </div>
    
    </div>
    
  </div>

</section>

<section id="section2">

<div class="container">
  
  	<div class="row">

	<div class="col-md-12" style="margin-top:100px; margin-bottom: 100px;">
    
  <div class="grid">
					<figure class="effect-sadie" style="width:30%">
						<img src="<?php bloginfo('template_url');?>/images/2.jpg" alt="img02"/>
						<figcaption>
							<h2>About Marian</h2>
							<p>A brief bit of text in here...</p>
							<a href="/about-marian-armitage/">View more</a>
						</figcaption>			
					</figure>
					<figure class="effect-sadie" style="width:30%">
						<img src="<?php bloginfo('template_url');?>/images/14.jpg" alt="img14"/>
						<figcaption>
							<h2>The Book</h2>
							<p>A brief bit of text in here...</p>
							<a href="#">View more</a>
						</figcaption>			
					</figure>
                    
                    <figure class="effect-sadie" style="width:30%">
						<img src="<?php bloginfo('template_url');?>/images/2.jpg" alt="img02"/>
						<figcaption>
							<h2>Recent Articles</h2>
							<p>A brief bit of text in here...</p>
							<a href="/recent-articles/">View more</a>
						</figcaption>			
					</figure>
					<figure class="effect-sadie">
						<img src="<?php bloginfo('template_url');?>/images/14.jpg" alt="img14"/>
						<figcaption>
							<h2>Recipes</h2>
							<p>A brief bit of text in here...</p>
							<a href="#">View more</a>
						</figcaption>			
					</figure>
                    <figure class="effect-sadie">
						<img src="<?php bloginfo('template_url');?>/images/14.jpg" alt="img14"/>
						<figcaption>
							<h2>Gallery</h2>
							<p>A brief bit of text in here...</p>
							<a href="#">View more</a>
						</figcaption>			
					</figure>
                    
				</div>
    
    </div>
    
    </div>
    
  </div>

</section>

<?php get_footer(); ?>